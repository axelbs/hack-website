import pymongo as mongo
import bcrypt
import os

from operator import itemgetter

from werkzeug import secure_filename
from flask import (Flask, render_template, flash, redirect, session,
                   url_for, request, Blueprint, request, jsonify, json)

args = {}
app = Flask(__name__, **args)
app.config.from_pyfile('conf/config.py')
app.config.update(SECRET_KEY='development key')

UPLOAD_FOLDER = '/tmp/programming'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'raw'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

mongo_url = "10.0.0.20"


def init_pages(app):
    @app.route('/')
    def index():
        return redirect(url_for('render_main'))

    @app.route('/login', methods=['GET', 'POST'])
    def login():
        error = None
        conn = mongo.MongoClient(mongo_url, w=1).scrum
        if request.method == 'POST':
            username = request.form['username']

            if username.strip() == '':
                error = "wrong username asss"
                return render_template('login.html', error=error)
            password = request.form['password']
            password = password.encode('utf-8')
            print(username)
            print(password)

            try:
                result = conn.users.find(
                    {"email": username}, {"password_bc": 1}
                )
                pw_hash = result[0]['password_bc']
                pw_hash = pw_hash.encode('utf-8')
                print "found user"
                print("pass: {}".format(pw_hash))
                if bcrypt.hashpw(password, pw_hash) == pw_hash:
                    session['login'] = True
                    session['username'] = request.form['username']
                    flash('You are logged in')
                    return redirect(url_for('render_main'))
                else:
                    print "Wrong hash"
            except Exception as e:
                #result = conn.users.find_one({"email": username})
                #if not result:
                    #categories = {
                        #"web": {},
                        #"crypto": {},
                        #"afk": {},
                        #"reveng": {}
                    #}
                    #password_bc = bcrypt.hashpw(password, bcrypt.gensalt())
                    #conn.users.insert({
                        #"password_bc": password_bc,
                        #"email": username,
                        #"ch": categories,
                        #"score": 0,
                        #"user_id": username
                    #})
                    #session['login'] = True
                    #session['username'] = request.form['username']
                    #flash('You are logged in')
                    #return redirect(url_for('render_main'))
                #print("Exception hit: e {}".format(e))
                #print "No such user or passord is wrong: " + str(e)
                error = "Wrong password or something..."

        return render_template('login.html', error=error)

    @app.route('/logout', methods=['GET'])
    def logout():
        session.pop('login', None)
        flash('You were logged out')
        return redirect(url_for('login'))

    @app.route('/sprint-set-status', methods=['GET', 'POST'])
    def set_status():
        if not session.get('login'):
            return redirect(url_for('login'))

        from pprint import pprint
        pprint(request.get_json())

        team_name = request.get_json().get("name", None)
        sprint_id = request.get_json().get("sprintId", None)

        if team_name is None or sprint_id is None:
            return jsonify(error=True)

        conn = mongo.MongoClient(mongo_url, w=1).scrum

        foo = conn.teams.update(
            {
                "name": team_name,
                "sprints.id": int(sprint_id)
            },
            {
                "$set": {"sprints.$.status": "FDJKFSDJFKSDL"}
            }
        )
        pprint(foo)
        return jsonify(error=False)
        # return redirect(url_for('render_main'))

    @app.route('/main', endpoint='render_main', methods=['GET'])
    def render_main():
        conn = mongo.MongoClient(mongo_url, w=1).scrum
        ongoing_sprints = conn.teams.find(
            {"sprints.status": True},
            {"sprints.$": 1, "_id": 0, "display_name": 1, "name": 1}
        )
        if not session.get('login'):
            return redirect(url_for('login'))
        from pprint import pprint
        # pprint(list(ongoing_sprints))
        return render_template(
            'sprint.html',
            ongoing=ongoing_sprints
        )

    @app.route('/edit-teams', methods=['GET', 'POST'])
    def render_teams():
        if not session.get('login'):
            return redirect(url_for('login'))

        if request.method == 'POST':
            pass

        conn = mongo.MongoClient(mongo_url, w=1).scrum
        result = conn.teams.find()
        return render_template('teams.html', teams=result)

    @app.route('/edit-team/<team_name>', methods=['GET', 'POST'])
    def render_specific_team(team_name):
        if not session.get('login'):
            return redirect(url_for('login'))

        conn = mongo.MongoClient(mongo_url, w=1).scrum
        team = conn.teams.find_one({"name": team_name})

        if request.method == 'POST':
            name = request.form.get("name")
            email = request.form.get("email")

            new_member = {
                "name": name,
                "email": email
            }
            _id = team["_id"]
            conn.team.update(
                {"_id": _id},
                {
                    "$push": {
                        "members": new_member
                    }
                }
            )

        # db.getCollection('teams').find({"name": "uio-int"})
        return render_template(
            'specific_team.html',
            team=team.get("members", None),
            team_name=team.get("name", None)
        )

    @app.route('/gl', methods=['GET'])
    def render_gl():
        return render_template('web_gl.html')

    @app.route('/edit-teams', methods=['GET', 'POST'])
    def render_edit_teams():
        conn = mongo.MongoClient(mongo_url, w=1).scrum
        if request.method == 'POST':
            pass
        return render_template(
            'index.html', username=session['username']
        )
        # Just render the shit

    @app.route('/json_render', methods=['GET'])
    def json_test():
        some_static_data = {
            "Type": "Value",
            "A_string": "More_String",
            "a_list": [
                1,
                2,
                4,
                9
            ]
        }
        return jsonify(some_static_data)


init_pages(app)
