var host = window.location.hostname;
var proto = window.location.protocol;
var port = window.location.port;
var hostname = proto + "//" + host + ":" + port;

var modal = document.querySelector('.modal');
var closeButtons = document.querySelectorAll('.close-modal');

const fetchSprintActionBtn = document.querySelector('.sprint-action')
const addTeamBtn = document.querySelector('.open-modal')

//if (typeof addTeamBtn !== "undefined") {
	//addTeamBtn.addEventListener('click', function() {
	  //modal.classList.toggle('modal-open');
	//});
//}

//// set close modal behaviour
//for (i = 0; i < closeButtons.length; ++i) {
  //closeButtons[i].addEventListener('click', function() {
    //modal.classList.toggle('modal-open');
	//});
//}

//// close modal if clicked outside content area
//document.querySelector('.modal-inner').addEventListener('click', function() {
  //modal.classList.toggle('modal-open');
//});

//// prevent modal inner from closing parent when clicked
//document.querySelector('.modal-content').addEventListener('click', function(e) {
	//e.stopPropagation();
//});

// gets data from API and sets the content of #result div
const setSprintAction = function() {
  const teamName = fetchSprintActionBtn.getAttribute("data-name");
  const sprintId = fetchSprintActionBtn.getAttribute("data-sprint-id");

  fetch('/sprint-set-status', {
      method:"POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({"name": teamName, "sprintId": sprintId })
  })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      //result.innerText = JSON.stringify(data, null, 2)
    })
    .catch(error => console.log(error))
    location.reload();
}

// Eventlisteners

if (typeof fetchSprintActionBtn !== "undefined") {
	fetchSprintActionBtn.addEventListener('click', setSprintAction)
}

// set open modal behaviour
